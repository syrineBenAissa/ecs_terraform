resource "aws_ecs_service" "cluster" {
  name = "cluster"
  cluster = "${aws_ecs_cluster.cluster.id}"
  task_definition = "ecs_recommander_task"
  desired_count = 1
 //iam_role = "${aws_iam_role.ecs-service-role.arn}"
  iam_role = "${var.ecs_role}"

  load_balancer {
    elb_name = "test"
    target_group_arn = "arn:aws:elasticloadbalancing:eu-west-1:565146382141:loadbalancer/app/test/254b47d0a8f11720"
    container_name = "recommender_adp"
    container_port = "8001"
  }
}

/*resource "aws_iam_role" "new_ecs-service-role" {
  name = "ECS-EC2-Acess"
  arn  = "arn:aws:iam::565146382141:role/EC2-ECS-Access"
  assume_role_policy = ""
}*/


/*data "aws_alb_target_group" "new_test" {
  name = "test"
  arn = "arn:aws:elasticloadbalancing:eu-west-1:565146382141:loadbalancer/app/test/254b47d0a8f11720"
}*/

/*resource "aws_ecs_service" "cluster" {
  name            = "cluster"
  cluster         = "${aws_ecs_cluster.cluster.id}"
  task_definition = "ecs_recommander_task"
  #iam_role        = "EC2-ECS-Access"
  iam_role        = "arn:aws:iam::565146382141:role/EC2-ECS-Access"
  //depends_on      = ["aws_iam_role_policy.foo"]
  load_balancer {
    target_group_arn = "arn:aws:elasticloadbalancing:eu-west-1:565146382141:loadbalancer/app/test/254b47d0a8f11720"
    container_name   = "mongo"
    container_port   = 8080
  }
  desired_count   = 2
}*/

/*
resource "aws_iam_role" "iam"{
  arn = "arn:aws:iam::565146382141:role/EC2-ECS-Access"
  assume_role_policy = <<EOF
  {
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}*/
