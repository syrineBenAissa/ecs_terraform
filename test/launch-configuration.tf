resource "aws_launch_configuration" "cluster" {
  name                 = "new_ecs_terraform"
  image_id             = "ami-0e3f55b258034f4fc"
  instance_type        = "t3.nano"
  security_groups      = ["sg-4cee6b37"]
  key_name             = "syrine"
  iam_instance_profile = "EC2-ECS-Access"
  lifecycle {
    create_before_destroy = true
  }
}

data "aws_security_group" "selected" {
  id = "${var.security_group_id}"
}

resource "aws_autoscaling_group" "cluster" {
  name = "${aws_launch_configuration.cluster.name}-asg"
  launch_configuration = "${aws_launch_configuration.cluster.name}"
  vpc_zone_identifier = ["subnet-c2017ea5"]
  min_size = 1
  desired_capacity = 1
  max_size = 2
  health_check_type = "EC2"
  default_cooldown = 300
  wait_for_capacity_timeout = "1m"

  lifecycle {
    create_before_destroy = true
  }
}

/*resource "aws_iam_role" "ecs-service-role" {
  name = "ECS-EC2-Acess"
  //arn  = "arn:aws:iam::565146382141:role/EC2-ECS-Access"
  assume_role_policy = ""
}

resource "aws_alb_target_group" "test" {
  name = "test"
  //arn = "arn:aws:elasticloadbalancing:eu-west-1:565146382141:loadbalancer/app/test/254b47d0a8f11720"
}*/

/*data "aws_autoscaling_group" "cluster" {
  //name = "${aws_launch_configuration.cluster.name}-asg"
  //id = "${var.autoscaling_group}"
  name = "${aws_launch_configuration.cluster.name}-asg"
  launch_configuration = "${aws_launch_configuration.cluster.name}"
}*/

/*data "aws_autoscaling_groups" "groups" {
  filter {
    name = "key"
    values = ["new_ecs_terraform"]
  }
}*/

/*data "aws_ami" "ecs_optimized" {
    owners           = ["self"]
  filter {
    name   = "name"
    values = ["ECS-EC2-Acess"]
  }

}*/


/*resource "aws_iam_role" "developer" {
  //arn = "arn:aws:iam::565146382141:role/EC2-ECS-Access"
  assume_role_policy = <<EOF
  {
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}*/
